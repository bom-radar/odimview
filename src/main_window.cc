#include "config.h"
#include "main_window.h"

#include <bom/ellipsoid.h>
#include <bom/trace.h>
#include <bom/io/png.h>

using namespace bom;
using namespace bom::gui;

// this must match the number of key_<n> and keycol_<n> controls in the Glade file
constexpr auto key_label_count = 18;

constexpr auto nodata = std::numeric_limits<float>::quiet_NaN();
constexpr auto undetect = std::numeric_limits<float>::infinity();

constexpr auto is_nodata(float val) { return std::isnan(val); }
constexpr auto is_undetect(float val) { return std::isinf(val); }

static auto to_rgba(colour col)
{
  auto rgba = Gdk::RGBA{};
  rgba.set_red(col.r / 255.0);
  rgba.set_green(col.g / 255.0);
  rgba.set_blue(col.b / 255.0);
  rgba.set_alpha(col.a / 255.0);
  return rgba;
}

static auto from_rgba(Gdk::RGBA rgba)
{
  return colour(rgba.get_red() * 255, rgba.get_green() * 255, rgba.get_blue() * 255, rgba.get_alpha() * 255);
}

static auto parse_float_nothrow(std::string const& str)
{
  float val = 0.0f;
  sscanf(str.c_str(), "%f", &val);
  return val;
}

static auto determine_volume_type(io::odim::attribute_store const& attributes)
{
  if (auto iatt = attributes.find("object"); iatt != attributes.end())
  {
    auto object = iatt->get_string();
    if (object == "SCAN" || object == "PVOL")
      return sweep_type::scan;
    if (object == "AZIM")
      return sweep_type::azim;
    if (object == "ELEV")
      return sweep_type::elev;
    throw std::runtime_error{"Unknown object type"};
  }

  trace::warning("File is missing object type metadata.  Assuming PVOL.");
  return sweep_type::scan;
}

static auto determine_sweep_type(io::odim::attribute_store const& attributes)
{
  if (auto iatt = attributes.find("product"); iatt != attributes.end())
  {
    auto product = iatt->get_string();
    if (product == "SCAN")
      return sweep_type::scan;
    if (product == "AZIM")
      return sweep_type::azim;
    if (product == "RHI")
      return sweep_type::elev;
    throw std::runtime_error{"Unknown product type"};
  }

  trace::warning("Dataset is missing product type metadata.  Assuming SCAN.");
  return sweep_type::scan;
}

// get the magnitude of the minimum difference between two angles in degrees
static auto abs_angle_diff(angle a, angle b) -> angle
{
  // there has to be a qucker / better way to do this
  return 1_rad * (M_PI - std::fabs(std::fmod(std::fabs(b.radians() - a.radians()), 2 * M_PI) - M_PI));
}

/* determine the scale and offset that must be applied to raw moment values so that they map correctly into the range
 * of values needed for the passed combination color_map and type_prefs. */
static auto determine_color_map_scale_offset(colour_map const& map, type_prefs const& pref)
{
  auto& cols = map.colours();
  auto range_scale = (cols[cols.size() - 1].first - cols[0].first) / (pref.range_max - pref.range_min);
  auto range_offset = cols[0].first - (pref.range_min * range_scale);
  return std::make_pair(range_scale, range_offset);
}

sweep_model::sweep_model(io::odim::dataset const& scan)
  : type{determine_sweep_type(scan.attributes())}
  , swept_angles(scan.attributes()["nrays"].get_integer() + 1)
  , ranges(scan.attributes()["nbins"].get_integer() + 1)
  , has_data{false}
  , data{vec2z(ranges.size() - 1, swept_angles.size() - 1)}
{
  switch (type)
  {
  case sweep_type::scan:
    /* scan objects are reordered in the ODIM file to always contain the north most ray first and
     * then continue in ascending order.  this is the simplest option for dealing with the swept
     * angles since the hard work has already been done for us. */
    {
      fixed_angle = scan.attributes()["elangle"].get_real() * 1_deg;
      auto astart = scan.attributes().find("astart");
      auto azi_start = astart != scan.attributes().end() ? astart->get_real() * 1_deg : 0_deg;
      auto azi_scale = 360.0_deg / data.extents().y;
      for (size_t i = 0; i < swept_angles.size(); ++i)
        swept_angles[i] = (azi_start + azi_scale * i).normalize();
      // fix discontinuities in first and last ray
      if (swept_angles[0] > swept_angles[1])
        swept_angles[0] -= 360.0_deg;
      if (swept_angles[swept_angles.size() - 1] < swept_angles[swept_angles.size() - 2])
        swept_angles[swept_angles.size() - 1] += 360.0_deg;
      positive_sweep = true;
    }
    break;
  case sweep_type::azim:
    /* azimuth objects can sweep in either ascending or descending order, and can also straddle
     * the 359..0 boundary.  we use antspeed to distinguish the direction.  we also do NOT normalize
     * the angles to ensure that we have a monotonic array.  normalization should be performed by the
     * code that is using the array*/
    {
      fixed_angle = scan.attributes()["elangle"].get_real() * 1_deg;
      auto azi_start = scan.attributes()["startaz"].get_real() * 1_deg;
      auto azi_stop = scan.attributes()["stopaz"].get_real() * 1_deg;
      positive_sweep = scan.attributes()["antspeed"].get_real() > 0.0;
      auto azi_delta = (abs_angle_diff(azi_stop, azi_start) / data.extents().y) * (positive_sweep ? 1: -1);
      for (size_t i = 0; i < swept_angles.size(); ++i)
        swept_angles[i] = azi_start + i * azi_delta;
    }
    break;
  case sweep_type::elev:
    /* azimuth objects can sweep in either ascending or descending order, and can also straddle
     * the 359..0 boundary.  we use antspeed to distinguish the direction.  we also do NOT normalize
     * the angles to ensure that we have a monotonic array.  normalization should be performed by the
     * code that is using the array*/
    {
      fixed_angle = scan.attributes()["az_angle"].get_real() * 1_deg;
      auto ele_start = scan.attributes()["startel"].get_real() * 1_deg;
      auto ele_stop = scan.attributes()["stopel"].get_real() * 1_deg;
      positive_sweep = scan.attributes()["antspeed"].get_real() > 0.0;
      auto ele_delta = (abs_angle_diff(ele_stop, ele_start) / data.extents().y) * (positive_sweep ? 1: -1);
      for (size_t i = 0; i < swept_angles.size(); ++i)
        swept_angles[i] = ele_start + i * ele_delta;
    }
    break;
  }

  auto range_start = scan.attributes()["rstart"].get_real() * 1000.0;
  auto range_scale = scan.attributes()["rscale"].get_real();
  for (size_t i = 0; i < ranges.size(); ++i)
    ranges[i] = range_start + range_scale * i;
}

auto sweep_model::find_ray(angle val) const -> int
{
  switch (type)
  {
  case sweep_type::scan:
    {
      // rays guaranteed to be in strictly ascending order
      auto i = std::lower_bound(swept_angles.begin(), swept_angles.end(), val.normalize());
      if (i == swept_angles.end())
        return val >= swept_angles[0] ? 0 : -1;
      return (i - swept_angles.begin()) - 1;
    }
    break;
  case sweep_type::azim:
  case sweep_type::elev:
    if (positive_sweep)
    {
      // normalize our target angle into the range covered by the array
      while (val > swept_angles[swept_angles.size() - 1])
        val -= 360.0_deg;
      while (val < swept_angles[0])
        val += 360.0_deg;
      auto i = std::lower_bound(swept_angles.begin(), swept_angles.end(), val);
      if (i == swept_angles.end())
        return -1;
      return (i - swept_angles.begin()) - 1;
    }
    else
    {
      // normalize our target angle into the range covered by the array
      while (val < swept_angles[swept_angles.size() - 1])
        val += 360.0_deg;
      while (val > swept_angles[0])
        val -= 360.0_deg;
      auto i = std::lower_bound(swept_angles.begin(), swept_angles.end(), val, [](auto& l, auto& r) { return l > r; });
      if (i == swept_angles.end())
        return -1;
      return (i - swept_angles.begin()) - 1;
    }
    break;
  }
  throw std::runtime_error{"Unreachable"};
}

auto sweep_model::find_bin(float val) const -> int
{
  auto i = std::lower_bound(ranges.begin(), ranges.end(), val);
  if (i == ranges.end())
    return -1;
  return (i - ranges.begin()) - 1;
}

type_prefs::type_prefs()
  : background{0x44, 0x44, 0x44, 0xff}
  , nodata{0x00, 0x00, 0x00, 0xff}
  , undetect{0x22, 0x22, 0x22, 0xff}
  , scheme("Greyscale")
  , range_min(0.0f)
  , range_max(100.0f)
  , filter_min(-std::numeric_limits<float>::infinity())
  , filter_max(std::numeric_limits<float>::infinity())
{ }

type_prefs::type_prefs(std::string const& hint)
  : type_prefs{}
{
  if (hint == "DBZH" || hint == "DBZV" || hint == "TH" || hint == "TV")
  {
    scheme = "Reflectivity";
    range_min = -32.0;
    range_max = 60.0;
  }
  else if (hint == "VRAD" || hint == "VRADH" || hint == "VRADV" || hint == "VRADDH" || hint == "VRADDV")
  {
    scheme = "Velocity";
    range_min = -23.6;
    range_max = 23.6;
  }
  else if (hint == "WRAD" || hint == "WRADH" || hint == "WRADV")
  {
    scheme = "SpWidth";
    range_min = 0.0;
    range_max = 15.0;
  }
  else if (hint == "ZDR")
  {
    scheme = "ZDR";
    range_min = -6.0;
    range_max = 6.0;
  }
  else if (hint == "RHOHV")
  {
    scheme = "RhoHV";
    range_min = 0.0;
    range_max = 1.0;
  }
  else if (hint == "PHIDP")
  {
    scheme = "PhiDP";
    range_min = 0.0;
    range_max = 360.0;
  }
  else if (hint == "KDP")
  {
    scheme = "KDP";
    range_min = -2.0;
    range_max = 7.0;
  }
  else if (hint == "SNRH" || hint == "SNRV")
  {
    scheme = "Jet";
    range_min = 0.0;
    range_max = 50.0;
  }
  // bom specific...
  else if (hint == "DBZH_CLEAN")
  {
    scheme = "Reflectivity";
    range_min = -32.0;
    range_max = 60.0;
  }
  else if (hint == "CLASS")
  {
    scheme = "PID_NCAR";
    range_min = -0.5;
    range_max = 20.5;
  }
}

type_prefs::type_prefs(io::configuration const& config)
  : background{config["background"]}
  , nodata{config["nodata"]}
  , undetect{config["undetect"]}
  , scheme(config["scheme"])
  , range_min(config["range_min"])
  , range_max(config["range_max"])
  // filter settings are never saved / restored - too much potential for user accidents!
  , filter_min(-std::numeric_limits<float>::infinity())
  , filter_max(std::numeric_limits<float>::infinity())
{ }

auto type_prefs::store(io::configuration& config) const -> void
{
  config["background"] = background;
  config["nodata"] = nodata;
  config["undetect"] = undetect;
  config["scheme"] = scheme;
  config["range_min"] = range_min;
  config["range_max"] = range_max;
  // filter settings are never saved / restored - too much potential for user accidents!
}

main_window::main_window(Glib::RefPtr<Gtk::Builder> builder)
  : window{builder, "main_window"}
  , preferences_path_{std::filesystem::path{getenv("HOME")} / ".odimview"}
  , max_range_{0.0f}
  , obj_type_{sweep_type::scan}
  , zoom_level_{0}
  , rhi_ceiling_{20000.0f}
  , rhi_aspect_{1}
  , bscope_aspect_{0}
  , range_rings_{50000.0}
  , rings_ground_{true}
  , canvas_size_{0, 0}
  , pref_updating_{false}
  , restore_pan_{false}
  , pan_target_{0.5f, 0.5f}
  , pan_mouse_{0.5f, 0.5f}
  , win_meta_{builder, "meta_window"}
  , win_prefs_(builder, "prefs_window")
  , but_open_(get_widget<Gtk::ToolButton>("open"))
  , but_meta_(get_widget<Gtk::ToggleToolButton>("metadata"))
  , but_prefs_(get_widget<Gtk::ToggleToolButton>("preferences"))
  , but_ppi_(get_widget<Gtk::RadioToolButton>("ppi"))
  , but_rhi_(get_widget<Gtk::RadioToolButton>("rhi"))
  , but_bscope_(get_widget<Gtk::RadioToolButton>("bscope"))
  , but_first_(get_widget<Gtk::ToolButton>("first"))
  , but_prev_(get_widget<Gtk::ToolButton>("prev"))
  , but_next_(get_widget<Gtk::ToolButton>("next"))
  , but_last_(get_widget<Gtk::ToolButton>("last"))
  , but_save_image_(get_widget<Gtk::ToolButton>("save_image"))
  , lbl_index_(get_widget<Gtk::Label>("index_type"))
  , spin_index_(get_widget<Gtk::SpinButton>("index_value"))
  , cb_data_type_(get_widget<Gtk::ComboBoxText>("data_type"))
  , lbl_cursor_(get_widget<Gtk::Label>("cursor_info"))
  , viewport_(get_widget<Gtk::Viewport>("viewport"))
  , draw_(get_widget<Gtk::DrawingArea>("draw"))
  , txt_metadata_(win_meta_.get_widget<Gtk::TextView>("metadata_info"))
  , cb_scheme_(win_prefs_.get_widget<Gtk::ComboBoxText>("scheme"))
  , col_background_(win_prefs_.get_widget<Gtk::ColorButton>("background"))
  , col_nodata_(win_prefs_.get_widget<Gtk::ColorButton>("nodata"))
  , col_undetect_(win_prefs_.get_widget<Gtk::ColorButton>("undetect"))
  , ent_range_min_(win_prefs_.get_widget<Gtk::Entry>("range_min"))
  , ent_range_max_(win_prefs_.get_widget<Gtk::Entry>("range_max"))
  , ent_filter_min_(win_prefs_.get_widget<Gtk::Entry>("filter_min"))
  , ent_filter_max_(win_prefs_.get_widget<Gtk::Entry>("filter_max"))
  , scale_ceiling_(win_prefs_.get_widget<Gtk::Scale>("ceiling"))
  , scale_aspect_(win_prefs_.get_widget<Gtk::Scale>("aspect"))
  , scale_bscope_aspect_(win_prefs_.get_widget<Gtk::Scale>("bscope_aspect"))
  , lbl_ceiling_(win_prefs_.get_widget<Gtk::Label>("lbl_ceiling"))
  , lbl_aspect_(win_prefs_.get_widget<Gtk::Label>("lbl_aspect"))
  , lbl_bscope_aspect_(win_prefs_.get_widget<Gtk::Label>("lbl_bscope_aspect"))
  , cb_rings_(win_prefs_.get_widget<Gtk::ComboBoxText>("rings"))
  , cb_rings_ground_(win_prefs_.get_widget<Gtk::ComboBoxText>("rings_ground"))
  , but_save_pref_(win_prefs_.get_widget<Gtk::Button>("save_prefs"))
  , but_reset_pref_(win_prefs_.get_widget<Gtk::Button>("reset_prefs"))
{
  for (auto i = 0; i < key_label_count; ++i)
  {
    lbl_key_.push_back(&get_widget<Gtk::Label>(fmt::format("key_{}", i).c_str()));
    ebox_key_.push_back(&get_widget<Gtk::EventBox>(fmt::format("keycol_{}", i).c_str()));
  }

  // load preferences
  auto config = io::configuration{std::ifstream{preferences_path_}};
  rhi_ceiling_ = config.optional("rhi_ceiling", rhi_ceiling_);
  rhi_aspect_ = config.optional("rhi_aspect", rhi_aspect_);
  bscope_aspect_ = config.optional("bscope_aspect", bscope_aspect_);
  range_rings_ = config.optional("range_rings", range_rings_);
  rings_ground_ = config.optional("rings_ground", rings_ground_);
  if (auto config_types = config.find("types"))
    for (auto& config_type : config_types->object())
      type_prefs_.emplace(config_type.first, config_type.second);

  scale_ceiling_.set_value(rhi_ceiling_);
  scale_aspect_.set_value(rhi_aspect_);
  scale_bscope_aspect_.set_value(bscope_aspect_);
  cb_rings_.set_active_text(range_rings_ <= 0.0 ? "Off" : fmt::format("{:g}", range_rings_ / 1000.0));
  cb_rings_ground_.set_active_text(rings_ground_ ? "Ground Range" : "Slant Range");

  // hookup gui signals
  but_open_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_open));
  but_meta_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_metadata));
  but_prefs_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_preferences));
  but_ppi_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_view));
  but_rhi_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_view));
  but_bscope_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_view));
  but_first_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_first));
  but_prev_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_prev));
  but_next_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_next));
  but_last_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_last));
  but_save_image_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_save_image));
  spin_index_.signal_value_changed().connect(sigc::mem_fun(*this, &main_window::on_spin));
  cb_data_type_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_data_type));
  viewport_.signal_scroll_event().connect(sigc::mem_fun(*this, &main_window::on_scroll));
  draw_.signal_size_allocate().connect(sigc::mem_fun(*this, &main_window::on_draw_resize));
  draw_.signal_draw().connect(sigc::mem_fun(*this, &main_window::on_draw_draw));
  draw_.signal_motion_notify_event().connect(sigc::mem_fun(*this, &main_window::on_draw_motion));

  // buttonx_motion_mask doesn't seem to work unless you also specify button_press_mask...
  draw_.set_events(
        draw_.get_events()
      | Gdk::EventMask::POINTER_MOTION_MASK
      | Gdk::EventMask::POINTER_MOTION_HINT_MASK
      | Gdk::EventMask::BUTTON1_MOTION_MASK
      | Gdk::EventMask::BUTTON_PRESS_MASK);

  win_meta_.get_window().signal_hide().connect(sigc::mem_fun(*this, &main_window::on_meta_close));

  win_prefs_.get_window().signal_hide().connect(sigc::mem_fun(*this, &main_window::on_prefs_close));
  cb_scheme_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  col_background_.signal_color_set().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  col_nodata_.signal_color_set().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  col_undetect_.signal_color_set().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  ent_range_min_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  ent_range_max_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  ent_filter_min_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  ent_filter_max_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  scale_ceiling_.signal_value_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  scale_aspect_.signal_value_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  scale_bscope_aspect_.signal_value_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  cb_rings_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  cb_rings_ground_.signal_changed().connect(sigc::mem_fun(*this, &main_window::on_prefs_change));
  but_save_pref_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_prefs_save));
  but_reset_pref_.signal_clicked().connect(sigc::mem_fun(*this, &main_window::on_prefs_reset));

  // fill the color scheme combo
  vector<std::string> entries;
  for (auto const& entry : std::filesystem::directory_iterator{schemes_dir()})
  {
    if (entry.path().extension() == ".cmap")
    {
      auto filename = entry.path().filename().string();
      entries.push_back(filename.substr(0, filename.size() - 5));
    }
  }
  std::sort(entries.begin(), entries.end());
  for (auto& e : entries)
    cb_scheme_.append(e);
  cb_scheme_.set_active(0);
}

auto main_window::set_current_file(std::filesystem::path path) -> void
{
  auto file = std::filesystem::absolute(path);
  auto odim = unique_ptr<io::odim::file>{};
  auto sweeps = sweep_models{};
  auto origin = latlonalt{};
  auto beam_width = angle{};
  auto max_range = float{};
  auto obj_type = sweep_type::scan;

  bool first_open = cur_file_.empty();

  // try to read the file
  try
  {
    odim = make_unique<io::odim::file>(file, io_mode::read_only);

    for (size_t i = 0; i < odim->dataset_count(); ++i)
      sweeps.emplace_back(odim->dataset_open(i));

    origin.lat = odim->attributes()["lat"].get_real() * 1_deg;
    origin.lon = odim->attributes()["lon"].get_real() * 1_deg;
    origin.alt = odim->attributes()["height"].get_real();

    if (auto i = odim->attributes().find("beamwV"); i != odim->attributes().end())
      beam_width = i->get_real() * 1_deg;
    else
    {
      trace::warning("No beamwidth provided.  Defaulting to 1.0 degrees");
      beam_width = 1.0_deg;
    }

    max_range = 0.0f;
    for (auto& s : sweeps)
      max_range = std::max(max_range, s.ranges[s.ranges.size() - 1]);

    obj_type = determine_volume_type(odim->attributes());
  }
  catch (std::exception& err)
  {
    Gtk::MessageDialog msg(get_window(), "Failed to open file", false, Gtk::MESSAGE_ERROR);
    msg.set_secondary_text(fmt::format("Error reading file {}:\n{}", file.filename().native(), format_exception(err, 2)));
    msg.run();
  }

  // okay we read everything so commit the changes
  cur_file_ = std::move(file);
  odim_ = std::move(odim);
  sweeps_ = std::move(sweeps);
  origin_ = std::move(origin);
  beam_width_ = std::move(beam_width);
  max_range_ = std::move(max_range);
  obj_type_ = obj_type;

  // update controls
  get_window().set_title(cur_file_.filename().string());
  but_ppi_.set_visible(obj_type_ != sweep_type::elev);
  if (but_ppi_.get_active() && obj_type_ == sweep_type::elev)
    but_rhi_.set_active();
  update_metadata();
  update_index(first_open);
  update_size();
  update_data_types();
  reload_data();
  draw_.queue_draw();
}

auto main_window::set_current_file(navigation nav) -> void
{
  // build a list of all hdf5 files in the current directory
  auto files = vector<std::filesystem::path>();
  for (auto& entry : std::filesystem::directory_iterator{cur_file_.parent_path()})
    if (entry.path().extension() == ".h5")
      files.push_back(entry.path());

  // sanity check
  if (files.empty())
    return;

  // sort the list - assume files are named so that they will sort by time
  std::sort(files.begin(), files.end());

  // navigate to the file in question
  switch (nav)
  {
  case navigation::first:
    set_current_file(files.front());
    break;
  case navigation::prev:
    if (auto i = std::lower_bound(files.begin(), files.end(), cur_file_); i != files.begin())
      set_current_file(*(i - 1));
    break;
  case navigation::next:
    if (auto i = std::upper_bound(files.begin(), files.end(), cur_file_); i != files.end())
      set_current_file(*i);
    break;
  case navigation::last:
    set_current_file(files.back());
    break;
  }
}

auto main_window::on_open() -> void
{
  auto dialog = Gtk::FileChooserDialog{"Please choose one or more files", Gtk::FILE_CHOOSER_ACTION_OPEN};
  dialog.set_transient_for(get_window());
  dialog.set_select_multiple(false);

  dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("_Open", Gtk::RESPONSE_OK);

  auto filter_h5 = Gtk::FileFilter::create();
  filter_h5->set_name("HDF5 files");
  filter_h5->add_pattern("*.h5");
  dialog.add_filter(filter_h5);

  auto filter_all = Gtk::FileFilter::create();
  filter_all->set_name("All files");
  filter_all->add_pattern("*");
  dialog.add_filter(filter_all);

  if (dialog.run() != Gtk::RESPONSE_OK)
    return;

  set_current_file(dialog.get_filename());
}

auto main_window::on_metadata() -> void
{
  if (but_meta_.get_active())
    win_meta_.show();
  else
    win_meta_.hide();
}

auto main_window::on_preferences() -> void
{
  if (but_prefs_.get_active())
    win_prefs_.show();
  else
    win_prefs_.hide();
}

auto main_window::on_view() -> void
{
  update_index();
  update_size();
  draw_.queue_draw();
}

auto main_window::on_first() -> void
{
  set_current_file(navigation::first);
}

auto main_window::on_prev() -> void
{
  set_current_file(navigation::prev);
}

auto main_window::on_next() -> void
{
  set_current_file(navigation::next);
}

auto main_window::on_last() -> void
{
  set_current_file(navigation::last);
}

auto main_window::on_save_image() -> void
{
  if (!odim_)
    return;

  auto dialog = Gtk::FileChooserDialog{"Enter image file name", Gtk::FILE_CHOOSER_ACTION_SAVE};
  dialog.set_transient_for(get_window());

  dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);

  auto filter = Gtk::FileFilter::create();
  filter->set_name("PNG images");
  filter->add_pattern("*.png");
  dialog.add_filter(filter);

  if (dialog.run() != Gtk::RESPONSE_OK)
    return;

  try
  {
    auto offset = vec2i{0, 0};
    auto img = image{canvas_size_};

    if (but_ppi_.get_active())
      render_ppi(offset, img);
    else if (but_rhi_.get_active())
      if (obj_type_ == sweep_type::elev)
        render_rhi_native(offset, img);
      else
        render_rhi_pseudo(offset, img);
    else
      render_bscope(offset, img);

    io::png_write(dialog.get_filename(), img);
  }
  catch (std::exception& err)
  {
    Gtk::MessageDialog msg(get_window(), "Failed to save image", false, Gtk::MESSAGE_ERROR);
    msg.set_secondary_text(fmt::format("Error saving image {}:\n{}", dialog.get_filename(), format_exception(err, 2)));
    msg.run();
  }
}

auto main_window::on_spin() -> void
{
  update_size();
  draw_.queue_draw();
}

auto main_window::on_data_type() -> void
{
  // update current display preferences to match stored preferences for new data type
  pref_updating_ = true;
  auto type = std::string(cb_data_type_.get_active_text());
  auto& pref = type_prefs_.try_emplace(type, type).first->second;
  col_background_.set_rgba(to_rgba(pref.background));
  col_nodata_.set_rgba(to_rgba(pref.nodata));
  col_undetect_.set_rgba(to_rgba(pref.undetect));
  cb_scheme_.set_active_text(pref.scheme);
  ent_range_min_.set_text(fmt::format("{:.2f}", pref.range_min));
  ent_range_max_.set_text(fmt::format("{:.2f}", pref.range_max));
  ent_filter_min_.set_text(fmt::format("{:.2f}", pref.filter_min));
  ent_filter_max_.set_text(fmt::format("{:.2f}", pref.filter_max));
  pref_updating_ = false;

  on_prefs_change();

  // load the new data
  reload_data();
}

auto main_window::on_scroll(GdkEventScroll* event) -> bool
{
  auto old_zoom = zoom_level_;

  if (event->direction == GDK_SCROLL_UP)
    zoom_level_--;
  if (event->direction == GDK_SCROLL_DOWN)
    zoom_level_++;

  // we ignore second scroll events before the first pan has been restored this prevents glitching in the pan location
  if (zoom_level_ != old_zoom)
  {
    if (!restore_pan_)
    {
      pan_mouse_.x = (event->x - viewport_.get_hadjustment()->get_value()) / viewport_.get_allocated_width();
      pan_mouse_.y = (event->y - viewport_.get_vadjustment()->get_value()) / viewport_.get_allocated_height();
      pan_target_.x = event->x / canvas_size_.x;
      pan_target_.y = event->y / canvas_size_.y;
      restore_pan_ = true;
    }
  }

  update_size();
  draw_.queue_draw();
  return false;
}

auto main_window::on_draw_resize(Gtk::Allocation& size) -> void
{
  if (restore_pan_)
  {
    viewport_.get_hadjustment()->set_value((pan_target_.x * canvas_size_.x) - (pan_mouse_.x * viewport_.get_allocated_width()));
    viewport_.get_vadjustment()->set_value((pan_target_.y * canvas_size_.y) - (pan_mouse_.y * viewport_.get_allocated_height()));
    restore_pan_ = false;
  }
}

auto main_window::on_draw_draw(const Cairo::RefPtr<Cairo::Context>& cc) -> bool
{
  auto regions = std::vector<Cairo::Rectangle>{};
  cc->copy_clip_rectangle_list(regions);
  for (auto& region : regions)
  {
    auto offset = vec2i(region.x, region.y);
    auto img = image{vec2z(region.width, region.height)};

    if (but_ppi_.get_active())
      render_ppi(offset, img);
    else if (but_rhi_.get_active())
      if (obj_type_ == sweep_type::elev)
        render_rhi_native(offset, img);
      else
        render_rhi_pseudo(offset, img);
    else
      render_bscope(offset, img);

    // swap red and blue to keep cairo happy (can't find a way to specify RGBA instead of BGRA in the library)
    for (size_t i = 0; i < img.size(); ++i)
      std::swap(img.data()[i].r, img.data()[i].b);

    cc->set_source(
          Cairo::ImageSurface::create(
              reinterpret_cast<unsigned char*>(img.data())
            , Cairo::FORMAT_ARGB32
            , region.width
            , region.height
            , region.width * 4)
        , region.x
        , region.y);
    cc->rectangle(region.x, region.y, region.width, region.height);
    cc->fill();
  }

  return false;
}

auto main_window::on_draw_motion(GdkEventMotion* event) -> bool
{
  if (!odim_)
  {
    lbl_cursor_.set_text("");
    return false;
  }

  // get the pick coordinates
  vec2i position;
  auto allocation = draw_.get_allocation();
  draw_.get_pointer(position.x, position.y);
  position.x -= allocation.get_x();
  position.y -= allocation.get_y();

  // perform mouse dragging
  vec2i drag;
  viewport_.get_pointer(drag.x, drag.y);
  if (event->state & GDK_BUTTON1_MASK && drag_ != drag)
  {
    auto ha = viewport_.get_hadjustment();
    ha->set_value(ha->get_value() + (drag_.x - drag.x));
    auto va = viewport_.get_vadjustment();
    va->set_value(va->get_value() + (drag_.y - drag.y));
  }
  drag_ = drag;

  // update the cursor information
  cursor_info info;
  if (but_ppi_.get_active())
    pick_ppi(position, info);
  else if (but_rhi_.get_active())
    if (obj_type_ == sweep_type::elev)
      pick_rhi_native(position, info);
    else
      pick_rhi_pseudo(position, info);
  else
    pick_bscope(position, info);

  if (info.valid)
  {
    string value;
    if (is_nodata(info.value))
      value = "No Data";
    else if (is_undetect(info.value))
      value = "Undetect";
    else if (sweeps_[info.sweep].labels.empty())
      value = fmt::format("{:.2f}", info.value);
    else
    {
      // find the label closest to the data
      auto const& labels = sweeps_[info.sweep].labels;
      auto ival = int(std::lround(info.value));
      size_t index = 0;
      while (index < labels.size() && labels[index].value != ival)
        ++index;
      if (index == labels.size())
        value = fmt::format("{:.2f} (unknown)", info.value);
      else
        value = fmt::format("{} ({})", ival, labels[index].label);
    }

    lbl_cursor_.set_text(fmt::format(
            "{}\n{}\n{}\n\n{:.1f}\n{:.1f}\n{:.1f}\n{:.1f}\n\n{:.3f}\n{:.3f}\n{:.3f}\n\n{}"
          , info.sweep, info.ray, info.bin
          , info.elevation, info.azimuth, info.slant_range * 0.001, info.ground_range * 0.001
          , info.location.lat, info.location.lon, info.altitude * 0.001
          , value));
  }
  else
    lbl_cursor_.set_text("");

  return false;
}

auto main_window::on_meta_close() -> void
{
  but_meta_.set_active(false);
}

auto main_window::on_prefs_close() -> void
{
  but_prefs_.set_active(false);
}

auto main_window::on_prefs_change() -> void
{
  /* ignore events that occur while we are adjusting the window.  this happens when we change data type.  if we
   * respond before all changes have been made, our old settings will overwrite the new prefs object we are trying
   * to install! */
  if (pref_updating_)
    return;

  auto& pref = type_prefs_[cb_data_type_.get_active_text()];

  pref.background = from_rgba(col_background_.get_rgba());
  pref.nodata = from_rgba(col_nodata_.get_rgba());
  pref.undetect = from_rgba(col_undetect_.get_rgba());
  pref.scheme = cb_scheme_.get_active_text();
  pref.range_min = parse_float_nothrow(ent_range_min_.get_text());
  pref.range_max = parse_float_nothrow(ent_range_max_.get_text());
  pref.filter_min = parse_float_nothrow(ent_filter_min_.get_text());
  pref.filter_max = parse_float_nothrow(ent_filter_max_.get_text());

  auto path = schemes_dir() / (pref.scheme + ".cmap");
  try
  {
    cur_map_ = colour_map{io::configuration{std::ifstream{path}}};
  }
  catch (std::exception& err)
  {
    Gtk::MessageDialog msg(get_window(), "Failed to load colour scheme", false, Gtk::MESSAGE_ERROR);
    msg.set_secondary_text(fmt::format("Error loading scheme {}:\n{}", path.string(), format_exception(err, 2)));
    msg.run();
  }

  rhi_ceiling_ = scale_ceiling_.get_value();
  rhi_aspect_ = scale_aspect_.get_value();
  bscope_aspect_ = scale_bscope_aspect_.get_value();
  if (auto rings_str = string(cb_rings_.get_active_text()); rings_str == "Off")
    range_rings_ = 0.0;
  else
    range_rings_ = from_string<double>(rings_str) * 1000.0;
  rings_ground_ = string(cb_rings_ground_.get_active_text()) == "Ground Range";

  lbl_ceiling_.set_text(fmt::format("{}", rhi_ceiling_));
  lbl_aspect_.set_text(fmt::format("{}", rhi_aspect_));
  lbl_bscope_aspect_.set_text(fmt::format("{:.2f}", bscope_aspect_));

  update_legend();
  update_size();
  draw_.queue_draw();
}

auto main_window::on_prefs_save() -> void
{
  auto type = string(cb_data_type_.get_active_text());

  auto config = io::configuration{std::ifstream{preferences_path_}};
  config["rhi_ceiling"] = rhi_ceiling_;
  config["rhi_aspect"] = rhi_aspect_;
  config["bscope_aspect"] = bscope_aspect_;
  config["range_rings"] = range_rings_;
  config["rings_ground"] = rings_ground_;
  type_prefs_[type].store(config["types"][type]);

  auto fout = std::ofstream{preferences_path_};
  fout << config;
}

auto main_window::on_prefs_reset() -> void
{
  pref_updating_ = true;
  auto type = std::string(cb_data_type_.get_active_text());
  auto& pref = type_prefs_.insert_or_assign(type, type).first->second;
  col_background_.set_rgba(to_rgba(pref.background));
  col_nodata_.set_rgba(to_rgba(pref.nodata));
  col_undetect_.set_rgba(to_rgba(pref.undetect));
  cb_scheme_.set_active_text(pref.scheme);
  ent_range_min_.set_text(fmt::format("{:.2f}", pref.range_min));
  ent_range_max_.set_text(fmt::format("{:.2f}", pref.range_max));
  ent_filter_min_.set_text(fmt::format("{:.2f}", pref.filter_min));
  ent_filter_max_.set_text(fmt::format("{:.2f}", pref.filter_max));

  scale_ceiling_.set_value(20000.0);
  scale_aspect_.set_value(5.0);
  scale_bscope_aspect_.set_value(0.0);
  cb_rings_.set_active_text("50");
  cb_rings_ground_.set_active_text("Ground Range");

  pref_updating_ = false;

  on_prefs_change();
}

static void print_meta(std::ostringstream& out, size_t indent, io::odim::attribute_store const& attributes)
{
  // find the minimum number of spaces needed for right hand column
  size_t col = 0;
  for (auto& att : attributes)
    col = std::max(col, att.name().size());
  col = std::max(size_t(16), col + 1);

  // print each item
  for (auto& att : attributes)
  {
    // indent
    for (size_t i = 0; i < indent; ++i)
      out << " ";

    // name
    out << att.name();

    // spaces till right column
    for (size_t i = att.name().size(); i < col; ++i)
      out << " ";

    // value
    switch (att.type())
    {
    case io::odim::attribute::data_type::boolean:
      out << att.get_boolean();
      break;
    case io::odim::attribute::data_type::integer:
      out << att.get_integer();
      break;
    case io::odim::attribute::data_type::real:
      out << att.get_real();
      break;
    case io::odim::attribute::data_type::string:
      out << att.get_string();
      break;
    case io::odim::attribute::data_type::integer_array:
      out << "(";
      for (auto v : att.get_integer_array())
        out << v << ",";
      out.seekp(-1, std::ios_base::cur);
      out << ")";
      break;
    case io::odim::attribute::data_type::real_array:
      out << "(";
      for (auto v : att.get_real_array())
        out << v << ",";
      out.seekp(-1, std::ios_base::cur);
      out << ")";
      break;
    default:
      break;
    }
    out << "\n";
  }
}

auto main_window::update_metadata() -> void
{
  if (!odim_)
  {
    txt_metadata_.get_buffer()->set_text("");
    return;
  }

  std::ostringstream out;
  print_meta(out, 0, odim_->attributes());
  for (size_t i = 0; i < odim_->dataset_count(); ++i)
  {
    out << "\ndataset " << (i + 1) << ":\n";

    auto dataset = odim_->dataset_open(i);
    print_meta(out, 4, dataset.attributes());

    for (size_t j = 0; j < dataset.data_count(); ++j)
    {
      out << "    data " << (j + 1) << ":\n";

      auto data = dataset.data_open(j);
      print_meta(out, 8, data.attributes());

      for (size_t k = 0; k < data.quality_count(); ++k)
      {
        out << "        quality " << (k + 1) << ":\n";
        auto quality = data.quality_open(k);
        print_meta(out, 12, quality.attributes());
      }
    }

    for (size_t j = 0; j < dataset.quality_count(); ++j)
    {
      out << "    quality " << (j + 1) << ":\n";

      auto data = dataset.quality_open(j);
      print_meta(out, 8, data.attributes());

      for (size_t k = 0; k < data.quality_count(); ++k)
      {
        out << "        quality " << (k + 1) << ":\n";
        auto quality = data.quality_open(k);
        print_meta(out, 12, quality.attributes());
      }
    }
  }
  txt_metadata_.get_buffer()->set_text(out.str());
}

auto main_window::update_data_types() -> void
{
  auto current_type = std::string(cb_data_type_.get_active_text());

  // clear the current options
  cb_data_type_.remove_all();

  if (!odim_)
    return;

  // build a list of type strings
  auto types = std::set<std::string>{};
  for (size_t i = 0; i < odim_->dataset_count(); ++i)
  {
    auto scan = odim_->dataset_open(i);
    for (size_t j = 0; j < scan.data_count(); ++j)
    {
      auto data = scan.data_open(j);
      types.insert(data.quantity());
      for (size_t k = 0; k < data.quality_count(); ++k)
        types.insert(data.quality_open(k).quantity());
    }
    for (size_t j = 0; j < scan.quality_count(); ++j)
    {
      auto data = scan.quality_open(j);
      types.insert(data.quantity());
      for (size_t k = 0; k < data.quality_count(); ++k)
        types.insert(data.quality_open(k).quantity());
    }
  }

  if (types.empty())
    return;

  // install the list in the combo box
  for (auto& type : types)
    cb_data_type_.append(type);

  // restore our active selection
  if (types.find(current_type) != types.end())
    cb_data_type_.set_active_text(current_type);
  else
    cb_data_type_.set_active_text(*types.begin());
}

auto main_window::update_index(bool first_open) -> void
{
  if (obj_type_ != sweep_type::elev && but_rhi_.get_active())
  {
    lbl_index_.set_text("Azimuth");
    spin_index_.set_range(0.0, 360.0);
    spin_index_.set_digits(1);
    spin_index_.set_wrap(true);
    spin_index_.set_increments(1.0, 1.0); // TODO - make increment match ray width
  }
  else
  {
    lbl_index_.set_text("Sweep");
    spin_index_.set_range(1, sweeps_.size());
    spin_index_.set_digits(0);
    spin_index_.set_wrap(false);
    spin_index_.set_increments(1, 1);

    /* if this is the first time opening a file then default to the sweep which has the lowest fixed angle.
     * this means that top-down scanned volumes will open at the lowest scan, which is generally what we want
     * to look at, rather than the birdbath, which is generally blank. */
    if (first_open && !sweeps_.empty())
    {
      auto ibest = 0;
      for (size_t i = 1; i < sweeps_.size(); ++i)
        if (sweeps_[i].fixed_angle < sweeps_[ibest].fixed_angle)
          ibest = i;
      spin_index_.set_value(ibest + 1);
    }
  }
}

auto main_window::update_size() -> void
{
  canvas_size_ = vec2i{0, 0};
  if (but_ppi_.get_active())
  {
    // zoom level 0 -> 1km x 1km pixels
    auto zoom = std::pow(0.7, zoom_level_);
    canvas_size_.x = zoom * ((max_range_ * 2) / 1000);
    canvas_size_.y = canvas_size_.x;
  }
  else if (but_rhi_.get_active())
  {
    // zoom level 0 -> 100m x 100m pixels
    auto zoom = std::pow(0.7, zoom_level_);
    canvas_size_.x = zoom * (max_range_ * 10 / 1000);
    canvas_size_.y = zoom * rhi_aspect_ * (rhi_ceiling_ * 10 / 1000.0f);
  }
  else
  {
    auto zoom = std::pow(0.7, zoom_level_);
    canvas_size_.y = zoom * (max_range_ * 2 / 1000);
    canvas_size_.x = zoom * std::pow(2, bscope_aspect_) * 360;
  }

  draw_.set_size_request(canvas_size_.x, canvas_size_.y);
}

auto main_window::update_legend() -> void
{
  auto& pref = type_prefs_.at(cb_data_type_.get_active_text());

  // we want to use white text on dark colors, and black on light colors
  /* convert HSV to HSL (we only need L) and then decide based on luminance. the threshold used is somewhat arbitrary
   * based on experimentation.  a W3C standard threshold of 0.179 is quoted, but leaves some labels too dark */
  auto label_col = [](auto col)
  {
    auto h = 0.0_deg;
    auto s = 0.0f, v = 0.0f, a = 0.0f;
    col.as_hsv(h, s, v, a);
    auto l = v * (1.0 - s / 2);
    return l > 0.4 ? standard_colours::black : standard_colours::white;
  };

  auto setup_slot = [&](auto i, auto str, auto col)
  {
    lbl_key_[i]->set_text(str);
    lbl_key_[i]->override_color(to_rgba(label_col(col)));
    ebox_key_[i]->override_background_color(to_rgba(col));
  };

  // update the legend labels
  /* labels don't have a background, so we embed each in an event box so we can set its background color. it's a bit of
   * an annoying thing that we need separate controls for the color and the lable, but GTK doesn't seem to provide a
   * nicer alternative out of the box.  this approach is suggested on various blogs as the defacto standard workaround.
   * note that we reserve the last two slots of the legend for our special flags of undetect and nodata. */
  auto [range_scale, range_offset] = determine_color_map_scale_offset(cur_map_, pref);
  auto min = std::max(pref.range_min, pref.filter_min);
  auto max = std::min(pref.range_max, pref.filter_max);
  auto normal_slots = key_label_count - 2;
  for (auto ikey = 0; ikey < normal_slots; ++ikey)
  {
    auto val = lerp(min, max, float(ikey) / (normal_slots - 1));
    auto col = cur_map_.evaluate((val * range_scale) + range_offset);
    setup_slot(ikey, fmt::format("{:0.3g}", val), col);
  }
  setup_slot(key_label_count - 2, "UD", pref.undetect);
  setup_slot(key_label_count - 1, "ND", pref.nodata);
}

auto main_window::reload_data() -> void
try
{
  if (!odim_)
    return;

  auto load_impl = [&](auto& sweep, auto& odim)
  {
    odim.read_unpack(sweep.data.data(), undetect, nodata);
    sweep.labels.clear();
    auto ilbls = odim.attributes().find("key_labels");
    auto ivals = odim.attributes().find("key_values");
    if (ilbls != odim.attributes().end() && ivals != odim.attributes().end())
    {
      try
      {
        auto kl = tokenize<string>(ilbls->get_string(), ",");
        auto kv = ivals->get_integer_array();
        if (kv.size() != kl.size())
          throw std::runtime_error{"Size mismatch between key_labels and key_values"};
        for (size_t i = 0; i < kl.size(); ++i)
          sweep.labels.emplace_back(kv[i], std::move(kl[i]));
      }
      catch (...)
      {
        sweep.labels.clear();
      }
    }
    sweep.has_data = true;
  };

  auto quantity = cb_data_type_.get_active_text();

  for (size_t i = 0; i < sweeps_.size(); ++i)
  {
    auto scan = odim_->dataset_open(i);

    auto& sweep = sweeps_[i];
    sweep.has_data = false;

    for (size_t j = 0; j < scan.data_count(); ++j)
    {
      auto data = scan.data_open(j);
      if (data.quantity() == quantity)
      {
        load_impl(sweep, data);
        break;
      }

      for (size_t k = 0; k < data.quality_count(); ++k)
      {
        auto quality = data.quality_open(k);
        if (quality.quantity() == quantity)
        {
          load_impl(sweep, quality);
          break;
        }
      }

      if (sweep.has_data)
        break;
    }

    if (sweep.has_data)
      continue;

    for (size_t j = 0; j < scan.quality_count(); ++j)
    {
      auto data = scan.quality_open(j);
      if (data.quantity() == quantity)
      {
        load_impl(sweep, data);
        break;
      }

      for (size_t k = 0; k < data.quality_count(); ++k)
      {
        auto quality = data.quality_open(k);
        if (quality.quantity() == quantity)
        {
          load_impl(sweep, quality);
          break;
        }
      }

      if (sweep.has_data)
        break;
    }
  }

  draw_.queue_draw();
}
catch (std::exception& err)
{
  Gtk::MessageDialog msg(get_window(), "Failed to read file", false, Gtk::MESSAGE_ERROR);
  msg.set_secondary_text(fmt::format("Error reading file {}:\n{}", cur_file_.filename().native(), format_exception(err, 2)));
  msg.run();
}

inline auto main_window::apply_range_rings(double ground_range, double slant_range, double resolution, colour& pixel) const -> void
{
  if (range_rings_ > 0.0 && std::fmod(rings_ground_ ? ground_range : slant_range, range_rings_) < resolution)
  {
    pixel.r ^= 0xff;
    pixel.g ^= 0xff;
    pixel.b ^= 0xff;
  }
}

auto main_window::render_ppi(vec2i offset, image& img) const -> void
{
  auto& pref = type_prefs_.at(cb_data_type_.get_active_text());

  // determine range mapping constants
  auto [range_scale, range_offset] = determine_color_map_scale_offset(cur_map_, pref);

  auto scan = spin_index_.get_value_as_int() - 1;
  if (!odim_ || scan < 0 || scan >= int(sweeps_.size()) || !sweeps_[scan].has_data)
  {
    img.fill(pref.background);
    return;
  }

  auto& sweep = sweeps_[scan];
  auto resolution = max_range_ / (canvas_size_.x * 0.5);
  auto beam = radar::beam_propagation{origin_.alt, sweep.fixed_angle};

  for (size_t y = 0; y < img.extents().y; ++y)
  {
    auto out = img[y];

    // determine our virtual canvas 'y'
    auto yy = offset.y + int(y);

    // if we are off the edge of the virtual canvas, just fill row with background
    if (yy < 0 || yy >= canvas_size_.y)
    {
      for (size_t x = 0; x < img.extents().x; ++x)
        out[x] = pref.background;
      continue;
    }

    // determine the radar centric y coordinate
    auto ycor = max_range_ - yy * resolution;

    // process each column in the output image row
    for (size_t x = 0; x < img.extents().x; ++x)
    {
      // determine our virtual canvas 'x'
      auto xx = offset.x + int(x);

      // if we are off the edge of the virtual canvas, just fill with background
      if (xx < 0 || xx >= canvas_size_.x)
      {
        out[x] = pref.background;
        continue;
      }

      // get the data point at this location
      auto xcor = xx * resolution - max_range_;

      // geographic coordinates
      auto bearing = 90_deg - atan2(ycor, xcor);
      auto ground_range = std::sqrt(xcor * xcor + ycor * ycor);
      auto slant_range = beam.slant_range(ground_range);

      auto ray = sweep.find_ray(bearing);
      auto bin = sweep.find_bin(slant_range);
      if (ray < 0 || bin < 0)
        out[x] = pref.background;
      else
      {
        auto val = sweep.data[ray][bin];
        if (is_undetect(val) || val < pref.filter_min || val > pref.filter_max)
          out[x] = pref.undetect;
        else if (is_nodata(val))
          out[x] = pref.nodata;
        else
          out[x] = cur_map_.evaluate((val * range_scale) + range_offset);
      }
      apply_range_rings(ground_range, slant_range, resolution, out[x]);
    }
  }
}

auto main_window::pick_ppi(vec2i position, cursor_info& info) const -> void
{
  info.valid = false;

  if (!odim_)
    return;

  info.sweep = spin_index_.get_value_as_int() - 1;
  if (info.sweep < 0 || info.sweep >= int(sweeps_.size()))
    return;

  auto& sweep = sweeps_[info.sweep];
  auto beam = radar::beam_propagation{origin_.alt, sweep.fixed_angle};
  auto resolution = max_range_ / (canvas_size_.x * 0.5);
  auto ycor = max_range_ - position.y * resolution;
  auto xcor = position.x * resolution - max_range_;

  auto bearing = 90_deg - atan2(ycor, xcor);
  auto ground_range = std::sqrt(xcor * xcor + ycor * ycor);
  auto slant_range = beam.slant_range(ground_range);

  info.ray = sweep.find_ray(bearing);
  info.bin = sweep.find_bin(slant_range);

  if (info.ray >= 0 && info.bin >= 0)
  {
    info.elevation = sweep.fixed_angle;
    info.azimuth = 0.5 * (sweep.swept_angles[info.ray] + sweep.swept_angles[info.ray + 1]);
    info.slant_range = 0.5 * (sweep.ranges[info.bin] + sweep.ranges[info.bin + 1]);
    info.ground_range = beam.ground_range(info.slant_range);
    info.altitude = beam.altitude(info.slant_range);
    info.location = wgs84.bearing_range_to_latlon(origin_, info.azimuth, info.ground_range);
    info.value = sweep.data[info.ray][info.bin];

    info.valid = true;
  }
}

auto main_window::render_rhi_native(vec2i offset, image& img) const -> void
{
  auto& pref = type_prefs_.at(cb_data_type_.get_active_text());

  // determine range mapping constants
  auto [range_scale, range_offset] = determine_color_map_scale_offset(cur_map_, pref);

  auto scan = spin_index_.get_value_as_int() - 1;
  if (!odim_ || scan < 0 || scan >= int(sweeps_.size()) || !sweeps_[scan].has_data)
  {
    img.fill(pref.background);
    return;
  }
  auto& sweep = sweeps_[scan];

  auto beam = radar::beam_propagation{origin_.alt};

  // determine pixel scales
  auto scale_x = max_range_ / canvas_size_.x;
  auto scale_y = rhi_ceiling_ / canvas_size_.y;

  // process each row in the output image
  for (size_t y = 0; y < img.extents().y; ++y)
  {
    auto out = img[y];

    // determine our virtual canvas 'y'
    auto yy = offset.y + int(y);

    // if we are off the edge of the virtual canvas, just fill row with background
    if (yy < 0 || yy >= canvas_size_.y)
    {
      for (size_t x = 0; x < img.extents().x; ++x)
        out[x] = pref.background;
      continue;
    }

    // process each column in the output image row
    for (size_t x = 0; x < img.extents().x; ++x)
    {
      // determine our virtual canvas 'x'
      auto xx = offset.x + int(x);

      // if we are off the edge of the virtual canvas, just fill with background
      if (xx < 0 || xx >= canvas_size_.x)
      {
        out[x] = pref.background;
        continue;
      }

      // ground range and altitude at this pixel
      auto ground_range = xx * scale_x;
      auto height = rhi_ceiling_ - (yy * scale_y);
      auto elev = beam.required_elevation_angle(ground_range, height);
      auto slant_range = beam.slant_range(elev, ground_range);

      // find the nearest ray and bin on that scan
      auto ray = sweep.find_ray(elev);
      auto bin = sweep.find_bin(slant_range);
      if (ray < 0 || bin < 0)
        out[x] = pref.background;
      else
      {
        auto val = sweep.data[ray][bin];
        if (is_undetect(val) || val < pref.filter_min || val > pref.filter_max)
          out[x] = pref.undetect;
        else if (is_nodata(val))
          out[x] = pref.nodata;
        else
          out[x] = cur_map_.evaluate((val * range_scale) + range_offset);
      }

      apply_range_rings(ground_range, slant_range, scale_x, out[x]);
    }
  }
}

auto main_window::pick_rhi_native(vec2i position, cursor_info& info) const -> void
{
  info.valid = false;

  info.sweep = spin_index_.get_value_as_int() - 1;
  if (info.sweep < 0 || info.sweep >= int(sweeps_.size()))
    return;

  auto beam = radar::beam_propagation{origin_.alt};

  // determine pixel scales
  auto scale_x = max_range_ / canvas_size_.x;
  auto scale_y = rhi_ceiling_ / canvas_size_.y;

  // ground range and altitude at this pixel
  auto ground_range = position.x * scale_x;
  auto height = rhi_ceiling_ - (position.y * scale_y);
  auto elev = beam.required_elevation_angle(ground_range, height);
  auto slant_range = beam.slant_range(elev, ground_range);

  // find the nearest ray and bin on that scan
  auto& sweep = sweeps_[info.sweep];
  info.ray = sweep.find_ray(elev);
  info.bin = sweep.find_bin(slant_range);
  if (info.ray >= 0 && info.bin >= 0)
  {
    info.elevation = elev;
    info.azimuth = sweep.fixed_angle;
    info.slant_range = 0.5 * (sweep.ranges[info.bin] + sweep.ranges[info.bin + 1]);
    info.ground_range = beam.ground_range(elev, info.slant_range);
    info.altitude = beam.altitude(elev, info.slant_range);
    info.location = wgs84.bearing_range_to_latlon(origin_, info.azimuth, info.ground_range);
    info.value = sweep.data[info.ray][info.bin];
    info.valid = true;
  }
}

auto main_window::render_rhi_pseudo(vec2i offset, image& img) const -> void
{
  auto& pref = type_prefs_.at(cb_data_type_.get_active_text());

  // determine range mapping constants
  auto [range_scale, range_offset] = determine_color_map_scale_offset(cur_map_, pref);

  auto beam = radar::beam_propagation{origin_.alt};
  auto bearing = spin_index_.get_value() * 1_deg;

  // maximum amount we are allowed off beam center
  auto max_delta = beam_width_ * 0.5;

  // determine pixel scales
  auto scale_x = max_range_ / canvas_size_.x;
  auto scale_y = rhi_ceiling_ / canvas_size_.y;

  // process each row in the output image
  for (size_t y = 0; y < img.extents().y; ++y)
  {
    auto out = img[y];

    // determine our virtual canvas 'y'
    auto yy = offset.y + int(y);

    // if we are off the edge of the virtual canvas, just fill row with background
    if (yy < 0 || yy >= canvas_size_.y)
    {
      for (size_t x = 0; x < img.extents().x; ++x)
        out[x] = pref.background;
      continue;
    }

    // process each column in the output image row
    for (size_t x = 0; x < img.extents().x; ++x)
    {
      // determine our virtual canvas 'x'
      auto xx = offset.x + int(x);

      // if we are off the edge of the virtual canvas, just fill with background
      if (xx < 0 || xx >= canvas_size_.x)
      {
        out[x] = pref.background;
        continue;
      }

      // ground range and altitude at this pixel
      auto ground_range = xx * scale_x;
      auto height = rhi_ceiling_ - (yy * scale_y);

      // determine the scan to use
      auto elev = beam.required_elevation_angle(ground_range, height);
      auto slant_range = beam.slant_range(elev, ground_range);

      // find the best scan to use
      auto best_delta = 180.0_deg;
      auto best = 0;
      for (size_t i = 0; i < sweeps_.size(); ++i)
      {
        if (!sweeps_[i].has_data)
          continue;

        auto delta = (sweeps_[i].fixed_angle - elev).abs();
        if (delta < best_delta)
        {
          best_delta = delta;
          best = i;
        }
      }

      // was the closest scan outside of the beam width?
      if (best_delta > max_delta)
      {
        out[x] = pref.background;
        apply_range_rings(ground_range, slant_range, scale_x, out[x]);
        continue;
      }

      // find the nearest ray and bin on that scan
      auto ray = sweeps_[best].find_ray(bearing);
      auto bin = sweeps_[best].find_bin(slant_range);
      if (ray < 0 || bin < 0)
        out[x] = pref.background;
      else
      {
        auto val = sweeps_[best].data[ray][bin];
        if (is_undetect(val) || val < pref.filter_min || val > pref.filter_max)
          out[x] = pref.undetect;
        else if (is_nodata(val))
          out[x] = pref.nodata;
        else
          out[x] = cur_map_.evaluate((val * range_scale) + range_offset);
      }

      apply_range_rings(ground_range, slant_range, scale_x, out[x]);
    }
  }
}

auto main_window::pick_rhi_pseudo(vec2i position, cursor_info& info) const -> void
{
  info.valid = false;

  auto beam = radar::beam_propagation{origin_.alt};
  auto bearing = spin_index_.get_value() * 1_deg;

  // maximum amount we are allowed off beam center
  auto max_delta = beam_width_ * 0.5;

  // determine pixel scales
  auto scale_x = max_range_ / canvas_size_.x;
  auto scale_y = rhi_ceiling_ / canvas_size_.y;

  // ground range and altitude at this pixel
  auto ground_range = position.x * scale_x;
  auto height = rhi_ceiling_ - (position.y * scale_y);

  // determine the scan to use
  auto elev = beam.required_elevation_angle(ground_range, height);
  auto slant_range = beam.slant_range(elev, ground_range);

  // find the best scan to use
  auto best_delta = 180.0_deg;
  auto best = 0;
  for (size_t i = 0; i < sweeps_.size(); ++i)
  {
    if (!sweeps_[i].has_data)
      continue;

    auto delta = (sweeps_[i].fixed_angle - elev).abs();
    if (delta < best_delta)
    {
      best_delta = delta;
      best = i;
    }
  }

  // was the closest scan outside of the beam width?
  if (best_delta > max_delta)
    return;

  // find the nearest ray and bin on that scan
  auto& sweep = sweeps_[best];
  info.sweep = best;
  info.ray = sweeps_[best].find_ray(bearing);
  info.bin = sweeps_[best].find_bin(slant_range);
  if (info.ray >= 0 && info.bin >= 0)
  {
    info.elevation = sweep.fixed_angle;
    info.azimuth = 0.5 * (sweep.swept_angles[info.ray] + sweep.swept_angles[info.ray + 1]);
    info.slant_range = 0.5 * (sweep.ranges[info.bin] + sweep.ranges[info.bin + 1]);
    info.ground_range = beam.ground_range(elev, info.slant_range);
    info.altitude = beam.altitude(elev, info.slant_range);
    info.location = wgs84.bearing_range_to_latlon(origin_, info.azimuth, info.ground_range);
    info.value = sweep.data[info.ray][info.bin];
    info.valid = true;
  }
}

auto main_window::render_bscope(vec2i offset, image& img) const -> void
{
  auto& pref = type_prefs_.at(cb_data_type_.get_active_text());

  // determine range mapping constants
  auto [range_scale, range_offset] = determine_color_map_scale_offset(cur_map_, pref);

  auto scan = spin_index_.get_value_as_int() - 1;
  if (!odim_ || scan < 0 || scan >= int(sweeps_.size()) || !sweeps_[scan].has_data)
  {
    img.fill(pref.background);
    return;
  }

  auto& sweep = sweeps_[scan];
  auto resolution_x = 360.0_deg / canvas_size_.x;
  auto resolution_y = max_range_ / canvas_size_.y;
  auto beam = radar::beam_propagation{origin_.alt, sweep.fixed_angle};

  for (size_t y = 0; y < img.extents().y; ++y)
  {
    auto out = img[y];

    // determine our virtual canvas 'y'
    auto yy = offset.y + int(y);

    // if we are off the edge of the virtual canvas, just fill row with background
    if (yy < 0 || yy >= canvas_size_.y)
    {
      for (size_t x = 0; x < img.extents().x; ++x)
        out[x] = pref.background;
      continue;
    }

    auto slant_range = (canvas_size_.y - yy - 1) * resolution_y;
    auto ground_range = beam.ground_range(slant_range);
    auto bin = sweep.find_bin(slant_range);
    if (bin < 0)
    {
      for (size_t x = 0; x < img.extents().x; ++x)
      {
        out[x] = pref.background;
        apply_range_rings(ground_range, slant_range, resolution_y, out[x]);
      }
      continue;
    }

    // process each column in the output image row
    for (size_t x = 0; x < img.extents().x; ++x)
    {
      // determine our virtual canvas 'x'
      auto xx = offset.x + int(x);

      // if we are off the edge of the virtual canvas, just fill with background
      if (xx < 0 || xx >= canvas_size_.x)
      {
        out[x] = pref.background;
        continue;
      }

      auto bearing = xx * resolution_x - 180.0_deg;
      auto ray = sweep.find_ray(bearing);
      if (ray < 0)
      {
        out[x] = pref.background;
        apply_range_rings(ground_range, slant_range, resolution_y, out[x]);
        continue;
      }

      // get the data point at this location
      auto val = sweep.data[ray][bin];
      if (is_undetect(val) || val < pref.filter_min || val > pref.filter_max)
        out[x] = pref.undetect;
      else if (is_nodata(val))
        out[x] = pref.nodata;
      else
        out[x] = cur_map_.evaluate((val * range_scale) + range_offset);

      apply_range_rings(ground_range, slant_range, resolution_y, out[x]);
    }
  }
}

auto main_window::pick_bscope(vec2i position, cursor_info& info) const -> void
{
  info.valid = false;

  if (!odim_)
    return;

  info.sweep = spin_index_.get_value_as_int() - 1;
  if (info.sweep < 0 || info.sweep >= int(sweeps_.size()))
    return;

  auto& sweep = sweeps_[info.sweep];
  auto resolution_x = 360.0_deg / canvas_size_.x;
  auto resolution_y = max_range_ / canvas_size_.y;
  auto beam = radar::beam_propagation{origin_.alt, sweep.fixed_angle};

  auto bearing = position.x * resolution_x - 180.0_deg;
  auto slant_range = (canvas_size_.y - position.y - 1) * resolution_y;

  info.ray = sweep.find_ray(bearing);
  info.bin = sweep.find_bin(slant_range);
  if (   info.ray < 0 || info.ray >= int(sweep.swept_angles.size()) - 1
      || info.bin < 0 || info.bin >= int(sweep.ranges.size()) - 1)
    return;

  info.elevation = sweep.fixed_angle;
  info.azimuth = 0.5 * (sweep.swept_angles[info.ray] + sweep.swept_angles[info.ray + 1]);
  info.slant_range = 0.5 * (sweep.ranges[info.bin] + sweep.ranges[info.bin + 1]);
  info.ground_range = beam.ground_range(info.slant_range);
  info.altitude = beam.altitude(info.slant_range);
  info.location = wgs84.bearing_range_to_latlon(origin_, info.azimuth, info.ground_range);
  info.value = sweep.data[info.ray][info.bin];

  info.valid = true;
}
