#pragma once

#include "window.h"
#include <bom/array1.h>
#include <bom/array2.h>
#include <bom/colour_map.h>
#include <bom/image.h>
#include <bom/latlon.h>
#include <bom/io/odim.h>
#include <bom/radar/beam_propagation.h>
#include <filesystem>

namespace bom {
namespace gui {

  enum class navigation
  {
      first
    , prev
    , next
    , last
  };

  struct val_lbl
  {
    val_lbl(int value, string label) : value{value}, label(std::move(label)) { }

    int     value;
    string  label;
  };

  enum class sweep_type
  {
      scan  // full 360 degree sweep
    , azim  // sector scan
    , elev  // rhi scan
  };

  struct sweep_model
  {
    sweep_model(io::odim::dataset const& scan);

    auto find_ray(angle val) const -> int;
    auto find_bin(float val) const -> int;

    sweep_type      type;           // type of sweep at odim level (full scan, sector or rhi)
    angle           fixed_angle;    // elevation for scan / sector, azimuth for RHI
    array1<angle>   swept_angles;   // azimuth at edges of each ray
    array1f         ranges;         // range at edges of each bin
    bool            positive_sweep; // is sweep angle increasing with time?
    bool            has_data;       // true if data is loaded
    array2f         data;           // data for currently selected data type
    vector<val_lbl> labels;         // value labels for discrete data types
  };

  struct type_prefs
  {
    type_prefs();
    type_prefs(std::string const& hint);
    type_prefs(io::configuration const& config);
    auto store(io::configuration& config) const -> void;

    colour      background;
    colour      nodata;
    colour      undetect;
    string      scheme;
    float       range_min;
    float       range_max;
    float       filter_min;
    float       filter_max;
  };

  using type_prefs_map = std::map<string, type_prefs>;

  struct cursor_info
  {
    bool    valid;
    int     sweep;
    int     ray;
    int     bin;
    angle   elevation;
    angle   azimuth;
    double  slant_range;
    double  ground_range;
    double  altitude;
    latlon  location;
    float   value;
  };

  class main_window : public window
  {
  public:
    main_window(Glib::RefPtr<Gtk::Builder> builder);

    auto set_current_file(std::filesystem::path path) -> void;
    auto set_current_file(navigation nav) -> void;

    // signal handlers for GUI controls
    auto on_open() -> void;
    auto on_metadata() -> void;
    auto on_preferences() -> void;
    auto on_view() -> void;
    auto on_first() -> void;
    auto on_prev() -> void;
    auto on_next() -> void;
    auto on_last() -> void;
    auto on_save_image() -> void;
    auto on_spin() -> void;
    auto on_data_type() -> void;
    auto on_scroll(GdkEventScroll* event) -> bool;
    auto on_draw_resize(Gtk::Allocation& size) -> void;
    auto on_draw_draw(const Cairo::RefPtr<Cairo::Context>& cc) -> bool;
    auto on_draw_motion(GdkEventMotion* event) -> bool;
    auto on_meta_close() -> void;
    auto on_prefs_close() -> void;
    auto on_prefs_change() -> void;
    auto on_prefs_save() -> void;
    auto on_prefs_reset() -> void;

  private:
    using odim_file_ptr = std::unique_ptr<io::odim::file>;
    using sweep_models = vector<sweep_model>;

  private:
    auto update_metadata() -> void;
    auto update_data_types() -> void;
    auto update_index(bool first_open = false) -> void;
    auto update_size() -> void;
    auto update_legend() -> void;

    auto reload_data() -> void;

    auto apply_range_rings(double ground_range, double slant_range, double resolution, colour& pixel) const -> void;

    auto render_ppi(vec2i offset, image& img) const -> void;
    auto pick_ppi(vec2i position, cursor_info& info) const -> void;

    auto render_rhi_native(vec2i offset, image& img) const -> void;
    auto pick_rhi_native(vec2i position, cursor_info& info) const -> void;

    auto render_rhi_pseudo(vec2i offset, image& img) const -> void;
    auto pick_rhi_pseudo(vec2i position, cursor_info& info) const -> void;

    auto render_bscope(vec2i offset, image& img) const -> void;
    auto pick_bscope(vec2i position, cursor_info& info) const -> void;

  private:
    std::filesystem::path   preferences_path_;
    type_prefs_map          type_prefs_;

    std::filesystem::path   cur_file_;
    odim_file_ptr           odim_;
    sweep_models            sweeps_;
    latlonalt               origin_;
    angle                   beam_width_;
    float                   max_range_;
    sweep_type              obj_type_;

    int                     zoom_level_;
    float                   rhi_ceiling_;
    int                     rhi_aspect_;
    float                   bscope_aspect_;
    double                  range_rings_;
    bool                    rings_ground_;
    colour_map              cur_map_;

    vec2i                   canvas_size_;
    vec2i                   drag_;

    bool                    pref_updating_; // used to suppress events when programatically changing display prefs
    bool                    restore_pan_;
    vec2f                   pan_target_;
    vec2f                   pan_mouse_;

    window                  win_meta_;
    window                  win_prefs_;

    Gtk::ToolButton&        but_open_;
    Gtk::ToggleToolButton&  but_meta_;
    Gtk::ToggleToolButton&  but_prefs_;
    Gtk::RadioToolButton&   but_ppi_;
    Gtk::RadioToolButton&   but_rhi_;
    Gtk::RadioToolButton&   but_bscope_;
    Gtk::ToolButton&        but_first_;
    Gtk::ToolButton&        but_prev_;
    Gtk::ToolButton&        but_next_;
    Gtk::ToolButton&        but_last_;
    Gtk::ToolButton&        but_save_image_;

    Gtk::Label&             lbl_index_;
    Gtk::SpinButton&        spin_index_;
    Gtk::ComboBoxText&      cb_data_type_;
    Gtk::Label&             lbl_cursor_;

    Gtk::Viewport&          viewport_;
    Gtk::DrawingArea&       draw_;

    std::vector<Gtk::Label*>    lbl_key_;
    std::vector<Gtk::EventBox*> ebox_key_;

    Gtk::TextView&          txt_metadata_;

    Gtk::ComboBoxText&      cb_scheme_;
    Gtk::ColorButton&       col_background_;
    Gtk::ColorButton&       col_nodata_;
    Gtk::ColorButton&       col_undetect_;
    Gtk::Entry&             ent_range_min_;
    Gtk::Entry&             ent_range_max_;
    Gtk::Entry&             ent_filter_min_;
    Gtk::Entry&             ent_filter_max_;
    Gtk::Scale&             scale_ceiling_;
    Gtk::Scale&             scale_aspect_;
    Gtk::Scale&             scale_bscope_aspect_;
    Gtk::Label&             lbl_ceiling_;
    Gtk::Label&             lbl_aspect_;
    Gtk::Label&             lbl_bscope_aspect_;
    Gtk::ComboBoxText&      cb_rings_;
    Gtk::ComboBoxText&      cb_rings_ground_;
    Gtk::Button&            but_save_pref_;
    Gtk::Button&            but_reset_pref_;
  };
}}
