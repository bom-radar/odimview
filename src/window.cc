#include "config.h"
#include "window.h"

#include <bom/string_utils.h>
#include <stdexcept>

using namespace bom::gui;

window::window(Glib::RefPtr<Gtk::Builder> builder, char const* object)
  : builder_{std::move(builder)}
{
  // retrieve the window widget
  Gtk::Window* ptr = NULL;
  builder_->get_widget(object, ptr);
  window_.reset(ptr);
  if (!window_)
    throw std::runtime_error{fmt::format("Failed to instanciate widget '{}'", object)};
}

window::~window()
{ }

auto window::show() -> void
{
  window_->show();
}

auto window::hide() -> void
{
  window_->hide();
}
