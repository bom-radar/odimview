#pragma once

#include <gtkmm.h>
#include <filesystem>
#include <memory>

namespace bom {
namespace gui {

  auto gui_file() -> std::filesystem::path;
  auto schemes_dir() -> std::filesystem::path;

  /// Base class for GUI windows
  class window
  {
  public:
    window(Glib::RefPtr<Gtk::Builder> builder, char const* object);

    window(const window& rhs) = delete;
    window(window&& rhs) = delete;
    window& operator=(const window& rhs) = delete;
    window& operator=(window&& rhs) = delete;

    virtual ~window();

    auto show() -> void;
    auto hide() -> void;

    auto get_window() -> Gtk::Window&
    {
      return *window_.get();
    }

    template <class T>
    auto get_object(char const* name) -> Glib::RefPtr<T>
    {
      return Glib::RefPtr<T>::cast_dynamic(builder_->get_object(name));
    }

    template <class T>
    auto get_widget(char const* name) -> T&
    {
      T* ptr;
      builder_->get_widget(name, ptr);
      return *ptr;
    }

  private:
    Glib::RefPtr<Gtk::Builder>    builder_;
    std::unique_ptr<Gtk::Window>  window_;
  };
}}
