#include "config.h"
#include "main_window.h"

#include <bom/string_utils.h>
#include <getopt.h>
#include <iostream>

using namespace bom;
using namespace bom::gui;

static auto determine_prefix_dir() -> std::filesystem::path
{
  return std::filesystem::canonical("/proc/self/exe").parent_path().parent_path();
}

auto bom::gui::gui_file() -> std::filesystem::path
{
  auto path = determine_prefix_dir() / "share/odimview/gui/odimview.glade";
  return path;
}

auto bom::gui::schemes_dir() -> std::filesystem::path
{
  auto path = determine_prefix_dir() / "share/odimview/schemes";
  return path;
}

class odimview : public Gtk::Application
{
public:
  // passing an empty string forces us to have a separate instances of the application
  odimview()
    : Gtk::Application{"", Gio::APPLICATION_HANDLES_OPEN}
    , builder_{Gtk::Builder::create_from_file(gui_file().native())}
  {
    Glib::set_application_name("ODIM Volume Viewer");

    add_main_option_entry(Gio::Application::OptionType::OPTION_TYPE_BOOL, "version", '\0', "Show version information");
    signal_handle_local_options().connect(sigc::mem_fun(*this, &odimview::on_handle_local_options));
  }

  auto on_activate() -> void override
  {
    main_window_ = std::make_unique<main_window>(builder_);
    add_window(main_window_->get_window());
    main_window_->get_window().present();
  }

  auto on_open(Gio::Application::type_vec_files const& files, Glib::ustring const& hint) -> void override
  {
    main_window_ = std::make_unique<main_window>(builder_);
    add_window(main_window_->get_window());
    main_window_->set_current_file(files[0]->get_path());
    main_window_->get_window().present();
  }

  auto on_handle_local_options(Glib::RefPtr<Glib::VariantDict> const& options) -> int
  {
    if (options->contains("version"))
    {
      std::cout << "odimview " ODIMVIEW_RELEASE_TAG << std::endl;
      return 0;
    }

    return -1;
  }

private:
  Glib::RefPtr<Gtk::Builder> builder_;
  unique_ptr<main_window> main_window_;
};

int main(int argc, char* argv[])
{
  auto app = odimview();
  return app.run(argc, argv);
}
