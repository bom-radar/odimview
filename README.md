# ODIM_H5 Volume Viewer

This software is a GTK+3 application which can be used to display polar radar
volumes files in the Opera Information Data Model for HDF5 (ODIM_H5) format.

Icon made by Freepik from www.flaticon.com
