# the user MUST supply --define="src_path mypath" to tell us where the main source directory is
%{!?src_path: %define src_path %{nil}}

# default to version 0.0.0 (indicates a local developer build)
# our CI job supplies an official version number for tagged releases
%{!?version: %define version 0.0.0}

# this allows the user to overwrite the build type using rpmbuild --define="build_type Debug"
# if using anything other than Release you will likely need to set QA_SKIP_BUILD_ROOT=1 before calling
# rpmbuild otherwise it will complain about buildroot paths in installed files
%{!?build_type: %define build_type Release}

# this allows the user to overwrite the base git url using rpmbuild --define="git_url my_new_url"
# useful for CI where we need a special deploy key based url
# note you must include the trailing ':' or '/'
%{!?git_url: %define git_url git@gitlab.bom.gov.au:}

Name:     odimview
Version:  %{version}
Release:  1%{?dist}
Summary:  Viewer for ODIM_H5 radar volume files
Group:    Applications/Radar
License:  Commonwealth of Australia, Bureau of Meteorology (ABN 92 637 533 532)
URL:      https://gitlab.bom.gov.au/radar/radar-fp
Vendor:   Bureau of Meteorology, Australia

Obsoletes: odimview < %{version}-%{release}
BuildRequires: gtkmm30-devel
Requires: gtkmm30

# prevent stripping of our binaries if we are performing a debug build
# it would be nice to support the standard debuginfo packages (using %%debug_package) but I
# haven't figured out how to get it working just yet...
%if "%{build_type}" != "Release"
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}
%endif

%define _prefix           /opt/odimview

# component releases
%define tag_bom_util      v1.4.0
%define tag_bom_core      v1.5.0

# find a suitable cmake binary (el7 installs cmake 3 in parallel as 'cmake3')
%define cmake_min 3.4.0
%define cmake_bin %(
  for bin in cmake cmake3; do
    if type ${bin} > /dev/null 2>&1; then
      bin_vers=$(${bin} --version | head -n1 | cut -d" " -f3)
      min_vers=$(echo -e "%{cmake_min}\\n${bin_vers}" | sort -V | head -n1)
      if [[ "%{cmake_min}" == "${min_vers}" ]]; then
        echo "${bin}"
        break
      fi
    fi
  done
)

#Source0:  https://gitlab.bom.gov.au/radar/bom-util/repository/archive.tar.gz?ref=%{tag_bom_util}#/bom-util-%{tag_bom_util}.tar.gz
#Source1:  https://gitlab.bom.gov.au/radar/bom-core/repository/archive.tar.gz?ref=%{tag_bom_core}#/bom-core-%{tag_bom_core}.tar.gz

%define nvdir %{name}-%{version}
# either below (staging prefix and no DESTDIR on installs), or no staging and DESTDIR
# which one is better?  do both link properly in all circumstances????
#define cmake_prefix_flags    -DCMAKE_INSTALL_PREFIX=%{_prefix} -DCMAKE_FIND_ROOT_PATH=%{buildroot} -DCMAKE_STAGING_PREFIX=%{buildroot}/%{_prefix}
%define cmake_prefix_flags    -DCMAKE_INSTALL_PREFIX=%{_prefix} -DCMAKE_FIND_ROOT_PATH=%{buildroot}
%define cmake_registry_flags  -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON -DCMAKE_FIND_PACKAGE_NO_SYSTEM_PACKAGE_REGISTRY=ON -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON
%define cmake_build_flags     -DCMAKE_BUILD_TYPE=%{build_type} %{cmake_prefix_flags} %{cmake_registry_flags}
%define cmake_install_flags   %{cmake_registry_flags}

%description
Simple GUI tool for viewing radar volume filese in the Opera Data Information
Model for HDF5 (ODIM_H5) format.

%prep
# verify that the user supplied src_path
%if "%{src_path}" == ""
  %{error:src_path must be defined by user (pass --define="src_path <path>" to rpmbuild)}
  exit 1
%endif

# verify we found a good cmake binary
%if "%{cmake_bin}" == ""
  %{error:no suitable cmake binary located.  set %%cmake_bin manually}
  exit 1
%endif

# cleanup before we setup
rm -rf %{nvdir}
mkdir %{nvdir}

cd %{nvdir}

git clone --depth=1 --branch=hdf5-1_10_7 %{git_url}mirrors/hdf5.git

git clone --branch=%{tag_bom_util} %{git_url}radar/bom-util.git
git clone --branch=%{tag_bom_core} %{git_url}radar/bom-core.git

%build
# cleanup any buildroot now - we need to install to it temporarily here so that
# bom-core can find bom-util and fp_selex_rb5 can find bom-core etc
rm -rf %{buildroot}

# this helps some of the components find each other during link when the link dependency is not transitive within
# the cmake configuration. for example odimview fails to link to hdf5 because the hdf5 dependency is not forwarded
# by bom-core.  we used to do that (by marking hdf5 as a PUBLIC dependency in bom-core), but it was a pain to get
# working for 'vanilla' builds where we are trying to use the system provided hdf5 (which has no cmake config)
export LD_LIBRARY_PATH=%{buildroot}%{_prefix}/lib64:%{buildroot}%{_prefix}/lib:${LD_LIBRARY_PATH}

cd %{nvdir}

mkdir hdf5/build
pushd hdf5/build
%{cmake_bin} .. %{cmake_build_flags} -DHDF5_ENABLE_Z_LIB_SUPPORT=ON -DBUILD_TESTING=OFF
make %{?_smp_mflags}
make install DESTDIR=%{buildroot}
popd

mkdir bom-util/build
pushd bom-util/build
%{cmake_bin} .. %{cmake_build_flags}
make %{?_smp_mflags}
make install DESTDIR=%{buildroot}
popd

mkdir bom-core/build
pushd bom-core/build
%{cmake_bin} .. %{cmake_build_flags}
make %{?_smp_mflags}
make install DESTDIR=%{buildroot}
popd

mkdir -p odimview/build
pushd odimview/build
%{cmake_bin} %{src_path} %{cmake_build_flags}
make %{?_smp_mflags}
make install DESTDIR=%{buildroot}
popd

%install
# buildroot is automatically cleaned up before this stage so we must redo the
# installation of each package

cd %{nvdir}

pushd hdf5/build
DESTDIR=%{buildroot} %{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=libraries -P cmake_install.cmake
DESTDIR=%{buildroot} %{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=hllibraries -P cmake_install.cmake
#{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=libraries -P cmake_install.cmake
#{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=hllibraries -P cmake_install.cmake
popd

pushd bom-util/build
DESTDIR=%{buildroot} %{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
#{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
popd

pushd bom-core/build
DESTDIR=%{buildroot} %{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
#{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
popd

pushd odimview/build
DESTDIR=%{buildroot} %{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
#{cmake_bin} %{cmake_install_flags} -DCMAKE_INSTALL_COMPONENT=runtime -P cmake_install.cmake
popd

# copy our mime type (odimview.xml) and application launcher (odimview.desktop) into the 'real' system locations
# rather than the /opt tree
install -d %{buildroot}/usr/share/mime/packages
install -m 755 %{buildroot}/%{_prefix}/share/mime/packages/odimview.xml %{buildroot}/usr/share/mime/packages/
install -d %{buildroot}/usr/share/applications
install -m 755 %{buildroot}/%{_prefix}/share/applications/odimview.desktop %{buildroot}/usr/share/applications/
install -d %{buildroot}/usr/share/icons/hicolor/48x48/apps
install -m 755 %{buildroot}/%{_prefix}/share/icons/hicolor/48x48/apps/odimview.png %{buildroot}/usr/share/icons/hicolor/48x48/apps/
install -d %{buildroot}/usr/share/icons/hicolor/256x256/apps
install -m 755 %{buildroot}/%{_prefix}/share/icons/hicolor/256x256/apps/odimview.png %{buildroot}/usr/share/icons/hicolor/256x256/apps/
install -d %{buildroot}/usr/share/icons/hicolor/scalable/mimetypes
install -m 755 %{buildroot}/%{_prefix}/share/icons/hicolor/scalable/mimetypes/application-x-odimview.svg %{buildroot}/usr/share/icons/hicolor/scalable/mimetypes/

# create a script to wrap calls to odimview from the command line
mkdir -p %{buildroot}/usr/bin
cat <<- 'EOF' > %{buildroot}/usr/bin/odimview
	#!/bin/bash
	LD_LIBRARY_PATH=/opt/odimview/lib64:/opt/odimview/lib:/${LD_LIBRARY_PATH} /opt/odimview/bin/odimview "${@}"
	EOF
chmod 0755 %{buildroot}/usr/bin/odimview

%files
%{_prefix}
/usr/share/mime/packages/odimview.xml
/usr/share/applications/odimview.desktop
/usr/share/icons/hicolor/48x48/apps/odimview.png
/usr/share/icons/hicolor/256x256/apps/odimview.png
/usr/share/icons/hicolor/scalable/mimetypes/application-x-odimview.svg
/usr/bin/odimview

#----------------------------------------------------------------------------------------------------------------------
%pre
#----------------------------------------------------------------------------------------------------------------------
exit 0

#----------------------------------------------------------------------------------------------------------------------
%post
#----------------------------------------------------------------------------------------------------------------------
gtk-update-icon-cache /usr/share/icons/hicolor
update-mime-database /usr/share/mime
update-desktop-database /usr/share/applications
exit 0

#----------------------------------------------------------------------------------------------------------------------
%preun
#----------------------------------------------------------------------------------------------------------------------

exit 0

#----------------------------------------------------------------------------------------------------------------------
%postun
#----------------------------------------------------------------------------------------------------------------------
gtk-update-icon-cache /usr/share/icons/hicolor
update-mime-database /usr/share/mime
update-desktop-database /usr/share/applications
exit 0

%changelog

